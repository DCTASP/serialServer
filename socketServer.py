import socket
import threading

class SocketServer:
    """A class for connecting communication protocals to TCP/IP sockets
    
    This class must be paired with another script.
    The other script must have communication function which will be passed into getData and sendData.
    
    Please and the end of your main deInit() to gracefully close all threads."""

    def __init__(self, port, getData, sendData, debug):

        self.port = port #This is the port that the socket is on
        self.getData = getData #this should be a method that retrives data to send to the client
        self.sendData = sendData #this is a method that should send data recived from the client
        self.debug = debug

        self.server = self.createServer(self.port)
        self.serverThread = threading.Thread(target=self.runServer) #Starts a thread that runs the server
        self.serverThread.daemon = True
        self.serverThread.start()

    
    def createServer(self, port):
        """Creates a server with a user given port"""
        
        serverSocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

        self.host = socket.gethostbyname(socket.gethostname()) #Host is determined by the devices IP
        self.host = "192.168.1.11"
        if(self.debug):
            print(self.host)
        serverSocket.bind((self.host, port))

        serverSocket.listen(20)

        print("Server is running.")

        return serverSocket

    def sendDataFromClient(self, message):
        """Sends data to the client form the communication protocol"""
        #if type(message) == type(b""): 
        self.sendData(message.decode("ascii"))
        #else:
        #    self.sendData(message)
    def getDataFromClient(self, connection):
        """gets data from the client sends it to the communication protocol"""
        connected = True
        while connected:
            data = self.getData()
            #print(type(data))  
            try:
                if type(data) != type(bytes("".encode("ascii"))):
                    #print("encoding")
                    encoded = bytes(data.encode("ascii"))
                    print(encoded)
                    connection.send(encoded)  
                    continue
                #print(type(data))  
                connection.send(data)
            except Exception as ex:
                print("exception:,",ex)
                connected = False


    def runServer(self):
        """Runs the server with the max of one client"""
        while True:
            connection, address = self.server.accept() #Blocks code until connection is made with a client
            clientConnection = True

            print("got a connection from %s" % str(address))

            msg = "Thank you for connecting" + "\r\n"
            connection.send(msg.encode('ascii'))
            
            sendingDataThread = threading.Thread(target = self.getDataFromClient, args = (connection,)) #Starts a thread to recieve data from client
            sendingDataThread.daemon = True
            sendingDataThread.start()
            
            while clientConnection:
                try:
                    msg = connection.recv(2048) #Blocks code until it recieves something from the client
                    #print("msg:",msg)
                    print(msg)
                    self.sendDataFromClient(msg)
                except Exception as ex:
                    print("client connection ex:",ex)
                    #print("%s disconnected" % str(address)) #if .recv() fails that means the client disconnected
                    clientConnection = False

            connection.close()
            #sendingDataThread._stop() #Client messeging thread closed becasue client disconnected
            sendingDataThread.join()

    def deinit(self): #closes all threads and processes
        self.server.close()
        threading._shutdown()
        self.serverThread._stop()
        self.serverThread._delete()

if __name__ == "__main__":
    def send():
        message = input().encode('ascii')
        return message

    def recieve(something):
        print(something.decode('ascii'))
    serv = SocketServer(1914,send,recieve,True)
    while True:
        pass
    serv.deinit()


