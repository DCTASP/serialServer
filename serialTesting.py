import serial
import threading
import time


class threadTest:
    def __init__(self, serialName, baud):
        self._ser = serial.Serial(port=serialName, baudrate=baud, timeout=1)


    def readSerial(self):
        return self._ser.readline()
    def writeSerial(self, message):
        self._ser.write(bytes(message.encode("ascii")))
        return


ser = threadTest("COM8", 115200)

print("after init of serial")
totalTime = 0
iterations = 15
for i in range(iterations):
    #ser.writeSerial("?FID\r")
    t = time.perf_counter_ns()
    print("before readline")
    msg = ser.readSerial()
    print("after readline")
    lineTime = (time.perf_counter_ns() - t)/ 1000000
    totalTime = totalTime + lineTime
    print(lineTime)
    print(msg)
    
print(totalTime / (iterations))


