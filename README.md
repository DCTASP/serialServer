# Serial Server

This is a collection of python scripts that will allow you to remotely connect to a rs232/usb device! 

# Dependencies
pip install pyserial

# How to use

Requires a computer connected to the rs232/usb device running the server script, then you can connect to
the socket that will be opened and use it along side a tcp to emulated serial software such as NetBurner 
to allow a serial connection to the remote device. 

**On remote server machine with rs232/usb connection**

    python serverSerial.py

**Client machine**

    Run socket to virtual com port software, enter the IP and port the server is running on

