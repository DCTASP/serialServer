import serial
import serial.tools.list_ports
from collections import deque
import threading
import time
import io
import timeit

class ReadLine:
    #credits to skoehler's comment on pyserial's github readline issue for this wrapper class with improved speed for reading lines
    def __init__(self, s, terminationChar="\r"):
        self.buf = bytearray()
        self.s = s
        self.terminationChar = bytes(terminationChar.encode("ascii"))[0]
    
    def readline(self):
        i = self.buf.find(self.terminationChar)
        #print("i =", i)
        if i >= 0:
            r = self.buf[:i+1]
            self.buf = self.buf[i+1:]
            return r
        while True:
            #print("reading line")
            i = max(1, min(2048, self.s.in_waiting))
            data = self.s.read(i)
            i = data.find(self.terminationChar)
            if i >= 0:
                r = self.buf + data[:i+1]
                self.buf[0:] = data[i+1:]
                
                return r.decode("ascii")
            else:
                self.buf.extend(data)


class SerialTransceiver:
    """
    The SerialTransceiver class is for async reading and writing
    to serial devices
    usage: 
    Connecting to port and starting async read/write
    serialTransceiver = SerialTransceiver("/dev/ttyACM0", _baud=115200)
    Reading from queue
        serialmsg = serialTransceiver.readLine()
    Writing to serial buffer (will be written async)
        serialTransceiver.writeLine("Message to the serial port")
    Clean up (Plz dont forget :) )
        serialTransceiver.deinit()
    """
    
    def __init__(self, _serialDeviceName="auto", _baud = 115200, _terminationChar="\r"):
        self._serialDevice = serial.Serial()
        self._incomingQueue = deque()
        self._outgoingQueue = deque()
        self._terminationChar = _terminationChar
        self._baud = _baud
        
        
        if _serialDeviceName == "auto":
            ports = list(serial.tools.list_ports.comports())
            self._serialDeviceName = ports[0][0]
        else:
            self._serialDeviceName = _serialDeviceName

        #print("Starting serial thread")
        self._serialThread = threading.Thread(target=self.SerialTransceiverThread)
        self._stop = False
        self._serialThread.start()
       
    
    #try to connect to the serial device. Will put a message in the queue if fails
    def tryConnect(self):
        try:
            #print("try to connect")
            self._serialDevice = self._connectToSerial()
            self._lineReader = ReadLine(self._serialDevice, terminationChar=self._terminationChar) 

            #self._serialDevice.open()
            return True
        except Exception as ex:
            #print("failed to connect to:", self._serialDeviceName)    
            self._incomingQueue.append("failed to connect to: " + self._serialDeviceName +" with exception:" + str(ex))
            return False

    def SerialTransceiverThread(self):
        #print("serial transceiver thread entered")
        while not self._stop:
            while not self._serialDevice.is_open: #keep trying to reconnect to the serial device every second till successful
                print("trying to connect:", self._serialDevice.is_open)
                if not self.tryConnect():
                    time.sleep(1)
                    continue
                else: 
                    print("connected successfully")
                    time.sleep(1)
            #print("starting read/write threads")
            
            readingThread = threading.Thread(target=self.ReadingThread, args=((self._serialDevice, self._incomingQueue)))
            
            writingThread = threading.Thread(target=self.WritingThread, args=((self._serialDeviceName, self._outgoingQueue)))
            writingThread = threading.Thread(target=self.WritingThread, args=((self._serialDevice, self._outgoingQueue)))
           
            writingThread.start()
            readingThread.start()
            
            
            if writingThread.is_alive:
                #print("join on writing thread")
                writingThread.join()
            if readingThread.is_alive:
                #print("join on reading thread")
                readingThread.join()
        

    def WritingThread(self, _serialDevice, _outgoingQueue):
        while _serialDevice.is_open:
            
            try:
                _serialDevice.write(self._outgoingQueue.popleft()) #get_nowait is used to keep from hanging when the queue is empty
                time.sleep(.01)
                _serialDevice.flush() #flush the outgoing buffer
                #print("wrote to serial device")
            except Exception as ex:
                #print("ex :", ex)
                time.sleep(.05)
            
        #print("returning from writing thread")
        

    def ReadingThread(self, _serialDevice, _incomingQueue):
        
        while self._serialDevice.is_open:
            #print("reading", _serialDevice.is_open)
            time.sleep(.01)
            
            if self._serialDevice.is_open: 
                try:
                    msg = self._lineReader.readline()
                    #print(msg)
                    if len(msg) < 2:
                        continue
                    else:
                        self._incomingQueue.append(msg)
                        #print("appended incoming message")
                    #_incomingQueue.append(self._serialDevice.readline()) #read in a line, and place it in the queue/buffer
                except Exception as ex:
                    #pass
                    time.sleep(.1)
                    print("reading thread exception: ex:", ex) #no data was read from the serial device

    def getBytesWaiting(self):
        if self._serialDevice.is_open:
            return self._serialDevice.in_waiting
        else:
            return 0

    def readLine(self):
        #returns the oldest message from 
        data = b""
        while self._incomingQueue.__len__() == 0:
            time.sleep(.01)
            continue
        
        data = self._incomingQueue.popleft()
        if type(data) == type(bytearray()):
            return data.decode("utf-8")

        return data

    #def getreadbuflen(self):
    #    return self._serialDevice.

    def writeLine(self, message):
        self._outgoingQueue.append(bytes((message.strip() + "\r").encode("ascii")))#place a message on the outgoing queue that will be written async when possible
        #print("message appended to outgoing buffer")
    def _connectToSerial(self):
        #connects to a serial device and returns a serial object
        #print("before serial init")
        #print(self._serialDeviceName, ":", self._baud)
        _ser = serial.Serial(port=self._serialDeviceName, baudrate=self._baud, timeout=1)
        #print("after serial init")
        _ser.write("\r".encode('utf-8'))
        #print("after serial write")
        _ser.flush()
        print("successfully connected to", self._serialDeviceName)
        return _ser

    def deinit(self):
        print("closing connections")
        self._stop = True
        self._serialDevice.close()

        if self._serialThread.is_alive:
            try:
                self._serialThread.join()
            except:
                pass


def testSerialTransceiverClass(_serialPort, _baud, _msgToWrite):
    serialTransceiver = SerialTransceiver(_serialPort, _baud)
    time.sleep(1)
    totalTime = 0
    iterations = 25
    serialTransceiver.writeLine(_msgToWrite)
    time.sleep(.1)

    for i in range(iterations):
        
        print("bytes waiting : ",serialTransceiver.getBytesWaiting())
        #t = time.perf_counter_ns()
        msg = serialTransceiver.readLine()
        #lineTime = (time.perf_counter_ns() - t)/ 1000000
        #totalTime = totalTime + lineTime
        #print("time taken to read line:", lineTime, "ms")

        print(msg)
    #print("average read time:", totalTime / (iterations), "ms")
    serialTransceiver.deinit()



if __name__ == "__main__":
    testSerialTransceiverClass("/dev/ttyACM0", 115200, "!r\r")#tests the serial class by writing and reading, and timing how long it takes
    
    
